import axios from "axios";
import { axiosClient } from "configs/axios.configs";

export const getBannerList = async () => {
  try {
    return await axiosClient.get("/QuanLyPhim/LayDanhSachBanner");
  } catch (error) {
    throw error;
  }
};

export const getMovieList = () => {
  return axiosClient.get("/QuanLyPhim/LayDanhSachPhim");
};
export const getDetailMovie = (maPhim) => {
  return axiosClient.get(`/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`);
};

export const getCinemaSystem = () => {
  return axiosClient.get("/QuanLyRap/LayThongTinLichChieuHeThongRap");
};
