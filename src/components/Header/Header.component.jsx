import React from "react";
import { Link } from "react-router-dom";
import "./Header.styles.scss";
import UserNavComponent from "./UserNav.component";

const HeaderComponent = () => {
  return (
    <div className="header shadow">
      <div className="header__wrap">
        <div className="header__logo">
          <Link to="/">
            <img
              src="https://scontent.fsgn5-6.fna.fbcdn.net/v/t1.15752-9/275713653_499042898583636_3759631675263291732_n.png?_nc_cat=108&ccb=1-5&_nc_sid=ae9488&_nc_ohc=Gu9LFgvgn84AX_P7ZTp&tn=TkzgopLjNnJ6i07H&_nc_ht=scontent.fsgn5-6.fna&oh=03_AVJsJdcJ3h3twpxN1KajvT-MGjuHghli2lj0pRhADKQSOg&oe=6280DE2F"
              alt=""
            />
          </Link>
        </div>
        <UserNavComponent />
      </div>
    </div>
  );
};

export default HeaderComponent;
