import { userLocalService } from "localService/localService";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { SET_USER_INFOR } from "store/constants/user.constants";

export default function UserNavComponent() {
  let { userInfor } = useSelector((state) => state.user);
  let dispatch = useDispatch();

  let handleLogout = () => {
    dispatch({
      type: SET_USER_INFOR,
      payload: null,
    });

    userLocalService.removeUserInfor();
  };
  return (
    <div>
      {userInfor ? (
        <div className="flex items-center space-x-5">
          <p className="text-red-600 text-lg font-medium m-0">
            {userInfor.hoTen}
          </p>

          <button
            onClick={handleLogout}
            className="bg-black text-white p-3 rounded"
          >
            Đăng xuất
          </button>
        </div>
      ) : (
        <div className="header__auth">
          <Link to="/login">
            <button className="btn-auth">Đăng Nhập</button>
          </Link>
          <Link to="/register">
            <button className="btn-auth">Đăng Ký</button>
          </Link>
        </div>
      )}
    </div>
  );
}
