import React from "react";
import { Form, Input, Button, Checkbox } from "antd";
import { postLogin } from "apis/user-management.apis";
import { useNavigate } from "react-router-dom";
import { message } from "antd";
import { useDispatch } from "react-redux";
import { postUserLoginAction } from "store/actions/user.action";

export default function LoginPage() {
  let history = useNavigate();
  let dispatch = useDispatch();

  const fetchData = async (data) => {
    let resutl = await postLogin(data);
    return resutl;
  };

  const onLoginSuccess = () => {
    message.success("Đăng nhập thành công");
    setTimeout(() => {
      history("/");
    }, 2000);
  };
  const onLoginFail = () => {
    message.error("Đăng nhập thất bại");
  };

  const onFinish = async (values) => {
    console.log(values);

    dispatch(postUserLoginAction(values, onLoginSuccess, onLoginFail));

    // if (result.statusCode == 200) {
    //   message.success("Đăng nhập thành công");
    //   setTimeout(() => {
    //     history("/");
    //   }, 2000);
    // } else {
    //   message.error("Đăng nhập thất bại");
    // }
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="bg-red-300 h-screen flex justify-center items-center ">
      <div className="container bg-white  p-10 rounded">
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 24,
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          layout="vertical"
        >
          <Form.Item
            label="Username"
            name="taiKhoan"
            rules={[
              {
                required: true,
                message: "Please input your username!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Mật khẩu"
            name="matKhau"
            rules={[
              {
                required: true,
                message: "Please input your username!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button className="bg-red-600 text-white rounded" htmlType="submit">
              Đăng nhập
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
