import moment from "moment";
import React from "react";

export default function MovieSchedual({ phim }) {
  // console.log({ phim });
  return (
    <div className="flex h-40 border-b p-2 space-x-3">
      <img className=" h-full w-28 object-cover" src={phim.hinhAnh} alt="" />

      <div>
        <p>{phim.tenPhim}</p>
        <div className=" grid grid-cols-3 gap-5 ">
          {phim.lstLichChieuTheoPhim.map(
            (item, index) =>
              index < 6 && (
                <span className=" bg-red-500 text-white  p-1 rounded">
                  {" "}
                  {moment(item.ngayChieuGioChieu).format("DDMM/YYYY  -  HH:mm")}
                </span>
              )
          )}
        </div>
      </div>
    </div>
  );
}
