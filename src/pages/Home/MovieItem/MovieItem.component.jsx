import React from "react";
import { Card } from "antd";
import { Link } from "react-router-dom";

const { Meta } = Card;
export default function MovieItemComponent({ movie }) {
  console.log("movie", movie);

  return (
    <Card
      //   hoverable
      cover={
        <img
          className="h-72 w-full object-cover"
          alt="example"
          src={movie.hinhAnh}
        />
      }
    >
      <Meta
        title={
          <p className="text-red-500">
            {movie.tenPhim.length > 30
              ? movie.tenPhim.slice(0, 30) + "..."
              : movie.tenPhim}
          </p>
        }
        description="www.instagram.com"
      />
      <button className=" bg-red-500   py-2 rounded w-full mt-3 hover:shadow-xl">
        <Link
          className="text-white w-full h-full block"
          to={`/detail/${movie.maPhim}`}
        >
          Mua vé
        </Link>
      </button>
    </Card>
  );
}
