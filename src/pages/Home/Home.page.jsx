// rafce
import { getMovieList } from "apis/movie-management.apis";
import BannerComponent from "components/Banner/Banner.component";
import React, { useEffect, useState } from "react";
import "./Home.styles.scss";
import MovieItemComponent from "./MovieItem/MovieItem.component";
import MovieTabs from "./MovieTabs/MovieTabs";
const HomePage = () => {
  let [dataMovies, setDataMoives] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      let result = await getMovieList();
      console.log(result);

      setDataMoives(result.content);
    };

    fetchData();
  }, []);

  return (
    <div>
      <div>
        <BannerComponent />
      </div>
      <div className="container mx-auto ">
        <div className="grid grid-cols-4 gap-4">
          {dataMovies.map((movie) => (
            <MovieItemComponent movie={movie} />
          ))}
        </div>

        <MovieTabs></MovieTabs>

        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
      </div>
    </div>
  );
};

export default HomePage;
