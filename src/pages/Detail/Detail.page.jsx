import { Progress } from "antd";
import { getDetailMovie } from "apis/movie-management.apis";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

export default function DetailPage() {
  // useEffect : hook ( hàm ) gồm 2 tham số ( st1 function, st2 dependencies )
  //  componentDidMount => truyền []
  //  componenDidUpdate => không truyền []

  // const [number, setNumber] = useState(0);
  // const [like, setLike] = useState(0);

  // useEffect(() => {
  //   console.log("one like");
  // }, [like, number]);

  // useEffect(() => {
  //   console.log(" auto run after setState");
  // }, []);
  // console.log("render");

  const [movie, setMovie] = useState({});

  let { id } = useParams();

  useEffect(() => {
    getDetailMovie(id)
      .then((res) => {
        console.log(res.content);

        setMovie(res.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  console.log(id);
  return (
    <div className="">
      <div className="container mx-auto  h-40 space-y-10 py-10">
        <div className="h-96 flex items-center space-x-10">
          <img
            className=" h-full w-1/5 shadow rounded "
            src={movie.hinhAnh}
            alt=""
          />

          <Progress
            type="circle"
            percent={movie.danhGia * 10}
            format={(diem) => `${diem / 10} điểm`}
            width={200}
          />
        </div>
        <p className="text-red-500 text-4xl">Tên phim : {movie.tenPhim}</p>
        <p>Mô tả: {movie.moTa}</p>
      </div>
      {/* <p>Number : {number}</p>

      <button
        onClick={() => {
          setNumber(number + 1);
        }}
        className="px-2 py-1 rounded bg-green-500"
      >
        Plus
      </button> */}
    </div>
  );
}
