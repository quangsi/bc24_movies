import { postLogin } from "apis/user-management.apis";
import { userLocalService } from "localService/localService";
import { SET_USER_INFOR } from "store/constants/user.constants";

export const postUserLoginAction = (data, onSuccess, onFail) => {
  return async (dispatch) => {
    try {
      const result = await postLogin(data);
      dispatch({
        type: SET_USER_INFOR,
        payload: result.content,
      });
      if (result.statusCode == 200) {
        userLocalService.setUserInfor(result.content);
        onSuccess();
      } else {
        onFail();
      }
    } catch (error) {
      return error;
    }
  };
};

// let sum = (a, b, onHello) => {
//   c = a + b;
//   onHello();
// };
// let sayHello = () => {
//   console.log("hello");
// };

// sum(4, 5, sayHello);
