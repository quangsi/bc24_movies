import { combineReducers } from "redux";
import movieReducer from "./movie.reducer";
import { userReducer } from "./user.reducer";

const rootReducer = combineReducers({
  movie: movieReducer,

  user: userReducer,
});

export default rootReducer;
