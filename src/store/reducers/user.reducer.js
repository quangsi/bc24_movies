import { userLocalService } from "localService/localService";
import { SET_USER_INFOR } from "store/constants/user.constants";

let initialState = {
  userInfor: userLocalService.getUserInfor(),
};

export const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_USER_INFOR: {
      state.userInfor = payload;
      return { ...state };
    }
    default:
      return state;
  }
};
