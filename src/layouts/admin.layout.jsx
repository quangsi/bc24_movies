import React from "react";
import { Outlet } from "react-router-dom";

const AdminLayout = () => {
  return (
    <>
      <header>
        <h1>Header Admin</h1>
      </header>
      <div>
        <section>
          <h1>SideBar</h1>
        </section>
        <section>
          <main>
            <Outlet />
          </main>
        </section>
      </div>
    </>
  );
};

export default AdminLayout;
